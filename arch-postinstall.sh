#!/bin/bash
#
# Name: arch-postinstall.sh
# Short-Description: ArchLinux Postinstall
# Version: 0.1.5

# Provides: arch-postinstall.sh, aur.sh, basics.sh, desktop.sh,
#           i18n.sh, settings.sh, wizard.sh, xorg.sh, yay.sh

# Requires: bash, cat, git, grep, nano, pacman, sed

# Devs: Pavel Sibal <entexsoft@gmail.com>
#
# ArchLinux Postinstall (arch-postinstall) is script for install and 
# basics configuration software ater fresh Arch Linux installation.
# Allow to you by the very easy way install all necessary software,
# desktops, display managers and AUR.
#
# ArchLinux Postinstall (arch-postinstall) is free software: you 
# can redistribute it and/or modify it under the terms of the 
# GNU General Public License as published by the Free Software 
# Foundation, either version 3 of the License, or (at your option) any 
# later version.
# You can get a copy of the license at www.gnu.org/licenses
#
# ArchLinux Postinstall (arch-postinstall) is distributed in the hope 
# that it will be useful, but WITHOUT ANY WARRANTY; without even the 
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# <http:/www.gnu.org/licenses/>.

# Run:
# chmod a+x arch-postinstall.sh ; ./arch-postinstall.sh

if [[ $(id -u) -ne 0 ]] ; then echo -e "\nWARNING: Run this program as root!!!\n" ; exit 1 ; fi
pacman -Syy
clear
PS3=$'\nSelect item please: '
items=("Wizards" "Basics stuffs" "Install Xorg" "Install Deskops" "AUR" "VirtualBox" "TeX & LaTeX" "Update" "Cleanup" "Shutdown" "Restart")
unset AURUSER

while true; do
    echo "################################"
    echo "#   Archlinux post-installer   #"
    echo "################################"
    echo
    select item in "${items[@]}" "Quit..."
    do
        case $REPLY in
            # Wizard
            1) bash bin/wizard.sh ; break ;;
            # Basics stuffs
            2) bash bin/basics.sh ; break ;;
            # Xorg
            3) bash bin/xorg.sh ; break ;;
            # Desktops
            4) bash bin/desktop.sh ; break ;;
            # AUR
            5) bash bin/aur.sh ; break ;;
            # VirtualBox
            6) bash bin/virtualbox.sh ; break ;;
            # TeX & LaTeX
            7) bash bin/latex.sh ; break ;;
            # Update
            8) pacman -Syyu; clear ; break ;;
            # Cleanup
            9) pacman -Scc ; clear; break ;;
            # Shutdown
            10) shutdown -h now ;;
            # Restart
            11) shutdown -r now ;;
            # Quit
            $((${#items[@]}+1))) clear; break 2;;
            *) clear ; break;;
        esac
    done
done
clear
