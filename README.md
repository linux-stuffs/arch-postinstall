# Arch Postinstall

ArchLinux Postinstall (arch-postinstall) is script for install and basics configuration software ater fresh Arch Linux installation.
Arch Postinstall allow to you by the very easy way install all necessary software, desktops, display managers and suport for AUR.

## INSTALLATION

```
git clone https://gitlab.com/linux-stuffs/arch-postinstall.git
cd arch-postinstall/
chmod a+x arch-postinstall.sh
```

## USAGE

```
./arch-postinstall.sh
```

## HOW TO FAST INSTALL ARCH LINUX

Download ISO file from https://archlinux.org/download/ and create boot media (DVD, USB, ...) How to create Bootable USB medium you can find [here](https://wiki.archlinux.org/title/USB_flash_installation_medium).
Then boot from the media and write commands:
```
pacman -Syy
pacman -Sy archlinux-keyring
archinstall
```
Fullfill there all these settings. Like "`Profile`" select "`minimal`" or "`xorg`"". In the Additional package put "`git nano wget bash-completion`" (space separated). 

Your screen shall look somehow like this:

```
Set/Modify the bellow options
  Archinstall language       set: English (100%)
  Keyboard layout            set: us
  Mirror region              set: ['Czechia']
  Locale language            set: en_US
  Locale encoding            set: UTF-8
  Drive(s)                   set: 1 Drive(s)
  Disk layout                set: 2 Partitions
  Bootloader                 set: grub-install
  Swap                       set: True
  Hostname                   set: archlinux01
  Root password              set: ****
  User account               set: 1 User(s)
  Profile                    set: Profile(xorg)
  Audio                      set: pulseaudio
  Kernels                    set: ['linux']
  Additional packages        set: ['nano', 'bash-completion', 'git', 'wget']
  Network configuration      set: Use NetworkManager
  Timezone                   set: UTC
  Automatic time sync (NTP)  set: True
  Optional repositories      set: ['multilib']
  
  Save configuration
> Install
  Abort
(Press "/" to search)
```

After installation and reboot log in like root and continue:

```
[root@archlinux01 ~]# git clone https://gitlab.com/linux-stuffs/arch-postinstall.git
[root@archlinux01 ~]# cd arch-postinstall/
[root@archlinux01 ~]# chmod a+x arch-postinstall.sh
[root@archlinux01 ~]# ./arch-postinstall.sh
```

```
################################
#   Archlinux post-installer   #
################################

1) Wizards	      3) Install Xorg	   5) AUR	        7) Update	     9) Shutdown	 11) Quit...
2) Basics stuffs      4) Install Deskops   6) VirtualBox        8) Cleanup	    10) Restart

Select item please: 1
```
Select `Wizards` (choice `1`) and then select some of the Desktops (as example `Xfce4 Desktop`)

```
#####################
#   WIZARDs MODEs   #
#####################

Wizard will automatically install software
with less users interactions.

1) Basics (no Desktop)	        4) CuteFish Desktop	       7) GNOME Desktop		     10) LXDE Desktop		    13) Openbox Desktop		   16) Additional software (AUR)
2) Budgie Desktop	        5) Deepin Desktop	       8) i3 Desktop		     11) LXQt Desktop		    14) UKUI Desktop		   17) Localized software
3) Cinnamon Desktop	        6) Enlightenment	       9) KDE (Plasma) Desktop	     12) Mate Desktop		    15) Xfce4 Desktop		   18) Return back...

Select item please: 15
```



