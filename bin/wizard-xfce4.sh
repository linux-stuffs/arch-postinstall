#!/bin/bash
#
# Name: wizard-xfce4.sh
# Version: 0.1.2
# Provides: wizard-xfce4.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
#	    lightdm.list systemctl xfce4.list 
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "##################################"
    echo "#   WIZARD MODE - XFCE4 DESKTOP  #"
    echo "##################################"
    echo -e "\nWizard will automatically install software and Xfce4 desktop"
    echo -e "with less users interactions.\n"
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/xfce4.list
		paclistins lists/lightdm.list
		systemctl enable lightdm.service
		sed -i 's/^\[Seat:\*\]/\[Seat:\*\]\ngreeter-setup-script=\/usr\/bin\/numlockx on/' /etc/lightdm/lightdm.conf
		yaylistins lists/aur-xfce4.list
		installdone
		bash ./bin/i18n.sh
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear
