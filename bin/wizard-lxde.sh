#!/bin/bash
#
# Name: wizard-lxde.sh
# Version: 0.1.2
# Provides: wizard-basics.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
#	    lxde.list lxdm.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "##################################"
    echo "#   WIZARD MODE - LXDE DESKTOP   #"
    echo "##################################"
    echo -e "\nWizard will automatically install software and LXDE desktop"
    echo -e "with less users interactions.\n"
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/lxde.list
		paclistins lists/lxdm.list
		systemctl enable lxdm.service
		installdone
		bash ./bin/i18n.sh; 
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear
