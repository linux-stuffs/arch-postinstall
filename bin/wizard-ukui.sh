#!/bin/bash
#
# Name: wizard-ukui.sh
# Version: 0.1.2
# Provides: wizard-ukui.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
#	    ukui.list sddm.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "##################################"
    echo "#   WIZARD MODE - UKUI DESKTOP   #"
    echo "##################################"
    echo -e "\nWizard will automatically install software and UKUI desktop"
    echo -e "with less users interactions.\n"
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/ukui.list
		paclistins lists/sddm.list
		systemctl enable sddm.service
		bash ./bin/i18n.sh
		installdone
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear

