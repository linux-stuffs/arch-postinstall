#!/bin/bash
#
# Name: wizard-i3.sh
# Version: 0.1.2
# Provides: wizard-i3.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
#	        i3.list lightdm.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "################################"
    echo "#   WIZARD MODE - I3 DESKTOP   #"
    echo "################################"
    echo -e "\nWizard will automatically install software and i3 desktop"
    echo -e "with less users interactions.\n"
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/i3.list
		paclistins lists/lightdm.list
		systemctl enable lightdm.service
		sed -i 's/^\[Seat:\*\]/\[Seat:\*\]\ngreeter-setup-script=\/usr\/bin\/numlockx on/' /etc/lightdm/lightdm.conf
		installdone
		bash ./bin/i18n.sh; 
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear
