#!/bin/bash
#
# Name: wizard-enlightenment.sh
# Version: 0.1.2
# Provides: wizard-enlightenment.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
#	    enlightenment.list lightdm.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "###################################"
    echo "#   WIZARD MODE - ENLIGHTENMENT   #"
    echo "###################################"
    echo -e "\nWizard will automatically install software and Enlightenment"
    echo -e "with less users interactions.\n"
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/enlightenment.list
		paclistins lists/lightdm.list
		systemctl enable lightdm.service
		sed -i 's/^\[Seat:\*\]/\[Seat:\*\]\ngreeter-setup-script=\/usr\/bin\/numlockx on/' /etc/lightdm/lightdm.conf
		installdone
		bash ./bin/i18n.sh; 
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear
