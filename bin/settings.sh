#!/bin/bash
#
# Name: settings.sh
# Version: 0.1.5
# Provides: settings.sh
#
# Requires: bash, locale-gen, nano
# Devs: Pavel Sibal <entexsoft@gmail.com>

usrgrp()
{
    clear
    while true; do
	echo "#####################"
	echo "#   User settings   #"
	echo "#####################"
	echo -e "\n$1 is into this groups:\n"
	groups $1
	echo
	read -p "Do you want add him into more standard groups? (y/n) " yn
        case $yn in 
            [yY] ) 
		usermod -G adm -a $1
		usermod -G lp -a $1
		usermod -G power -a $1
		usermod -G storage -a $1
		usermod -G audio -a $1
		usermod -G video -a $1
		break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
}

usrmod()
{
    clear
    while true; do
	echo "#####################"
	echo "#   User settings   #"
	echo "#####################"
	echo
	read -p "Do you want to proceed? (y/n) " yn

        case $yn in 
            [yY] ) 
		echo -e "\nYou can change description of user."
		echo -e "You will must to select someone from these usernames:\n"
		grep '^wheel:.*$' /etc/group | cut -d: -f4
		echo 
		read -p 'Type valid Username and press Enter: ' MYUSR
		read -p 'Type some user description and press Enter: ' MYDESC
		usermod -c $MYDESC $MYUSR
		usrgrp $MYUSR
		break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
}


PS3=$'\nSelect item please: '
items=("Network settings" "Locale" "vconsole" "User Modification")

clear
while true; do
    echo "#######################"
    echo "#   Basics settings   #"
    echo "#######################"
    echo
    select item in "${items[@]}" "Return back..."
    do
        case $REPLY in
        # Network settings
        1) 
		nano /etc/hostname
		HN=$(cat /etc/hostname | xargs)
		echo -e "127.0.0.1\t$HN\t\t$HN.localdomain" >> /etc/hosts
		echo -e "127.0.0.1\tlocalhost\tlocalhost.localdomain" >> /etc/hosts
		echo -e "::1\t\tlocalhost\tlocalhost.localdomain" >> /etc/hosts
		nano /etc/hosts
		clear
		break;;
        
        # Locale
        2)
		nano /etc/locale.gen
		nano /etc/locale.conf
		locale-gen
		clear
		break;;
        
        # vconsole
        3)
		nano /etc/vconsole.conf
		break;;
	    
	    # User modification
	    4)
		usrmod
		clear
		break;;
        
        # Return back
        $((${#items[@]}+1))) break 2;;
        *) clear; break;
        esac
    done
done
clear

