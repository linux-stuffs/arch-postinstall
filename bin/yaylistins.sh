#!/bin/bash
#
# Name: yaylisting.sh
# Version: 0.1.1
# Provides: yaylisting.sh
#
# Requires: bash, cat, yay, xargs
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

cat $1 | xargs yay -S --noconfirm;
