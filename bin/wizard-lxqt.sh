#!/bin/bash
#
# Name: wizard-lxqt.sh
# Version: 0.1.2
# Provides: wizard-lxqt.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
# 	    lxqt.list sddm.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "##################################"
    echo "#   WIZARD MODE - LXQt DESKTOP   #"
    echo "##################################"
    echo -e "\nWizard will automatically install software and LXQt desktop"
    echo -e "with less users interactions.\n"
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/lxqt.list
		paclistins lists/sddm.list
		systemctl enable sddm.service
		installdone
		bash ./bin/i18n.sh; 
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear
