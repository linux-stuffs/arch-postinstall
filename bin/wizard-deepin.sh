#!/bin/bash
#
# Name: wizard-deepin.sh
# Version: 0.1.2
# Provides: wizard-deepin.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
#		    deepin.list lightdm.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "###################################"
    echo "#   WIZARD MODE - DEEPIN DESKTOP  #"
    echo "###################################"
    echo -e "\Wizard will automatically install software and Deepin desktop"
    echo -e "with less users interactions.\n"
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/deepin.list
		paclistins lists/lightdm.list
		systemctl enable lightdm.service
		sed -i 's/^\[Seat:\*\]/\[Seat:\*\]\ngreeter-setup-script=\/usr\/bin\/numlockx on/' /etc/lightdm/lightdm.conf
		sed -i 's/^\#greeter-session=example-gtk-gnome/\#greeter-session=example-gtk-gnome\ngreeter-session=lightdm-deepin-greeter/' /etc/lightdm/lightdm.conf
		installdone
		bash ./bin/i18n.sh; 
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear
