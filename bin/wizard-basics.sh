#!/bin/bash
#
# Name: wizard-basics.sh
# Version: 0.1.2
# Provides: wizard-basics.sh
#
# Requires: bash grep read helper-installfunct.sh helper-wizards.sh
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "########################################"
    echo "#   WIZARD MODE - BASICS (no desktop)  #"
    echo "########################################"
    echo 
    echo "Wizard will automatically install software (except Desktop)"
    echo "with less users interactions."
    echo 
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		installdone
		bash ./bin/i18n.sh
		break;;
	    [nN] ) clear; exit;;
	    *) clear ; break;
    esac
done
clear
