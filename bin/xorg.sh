#!/bin/bash
#
# Name: xorg.sh
# Version: 0.1.2
# Provides: xorg.sh, develop.list, deskprograms.list, fonts.list, 
#           games.list, graphicssoft.list, gtkthemes.list, 
#           multimedia.list, networksoft.list, office.list, 
#           security.list, xorg-base.list, xorg-programs.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

xorgbase()
{
    clear
    while true; do
        echo "#######################################"
        echo "# Do you want to install Xorg (Base)? #"
        echo "#######################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/xorg-base.list
                installdone ; break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}

xorgprograms()
{
    clear
    while true; do
        echo "###########################################"
        echo "# Do you want to install Xorg (Programs)? #"
        echo "###########################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/xorg-programs.list
                installdone ; break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}

fonts()
{
    clear
    while true; do
        echo "#################################"
        echo "# Do you want to install Fonts? #"
        echo "#################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/fonts.list
                installdone ; break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}

deskprograms()
{
    clear
    while true; do
        echo "###################################################"
        echo "# Do you want to install basics Desktop programs? #"
        echo "###################################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/deskprograms.list
                installdone ; break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}

multimedia()
{
    clear
    while true; do
        echo "######################################################"
        echo "# Do you want to install basics Multimedia software? #"
        echo "######################################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/multimedia.list
                installdone ; break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}

networksoft()
{
    clear
    while true; do
        echo "###################################################"
        echo "# Do you want to install basics Network software? #"
        echo "###################################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/networksoft.list
                installdone ; break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}

graphicssoft()
{
    clear
    while true; do
        echo "####################################################"
        echo "# Do you want to install basics Graphics software? #"
        echo "####################################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/graphicssoft.list
                installdone ; break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}

office()
{
    clear
    while true; do
        echo "########################################"
        echo "# Do you want to install Office Suite? #"
        echo "########################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                libreofficechk
                paclistins lists/office.list
                installdone ; break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}

games()
{
    clear
    while true; do
        echo "######################################"
        echo "# Do you want to install some games? #"
        echo "######################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/games.list
                installdone ; break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}

develop()
{
    clear
    while true; do
        echo "###################################################"
        echo "# Do you want to install basics Develop software? #"
        echo "###################################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/develop.list
                installdone ; break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}

security()
{
    clear
    while true; do
        echo "#############################################"
        echo "# Do you want to install Security software? #"
        echo "#############################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/security.list
                installdone ; break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}


PS3=$'\nSelect item please: '
items=("Xorg base" "Xorg programs" "Fonts" "Desktop programs" "Multimedia" "Network software" "Graphics software" "Office software" "Games" "Develop" "Security software" "Localized software")
clear

while true; do
    echo "################################"
    echo "#   Archlinux Xorg Installer   #"
    echo "################################"
    echo
    select item in "${items[@]}" "Return back..."
    do
        case $REPLY in
	    # Xorg base
            1) xorgbase ; break;;
            # Xorg programs
            2) xorgprograms ; break;;
            # Fonts
            3) fonts ; break;;
            # Desktop programs
            4) deskprograms ; break;;
            # Multimedia
            5) multimedia ; break;;
            # Network software
            6) networksoft ; break;;
            # Graphics software
            7) graphicssoft ; break;;
            # Office software
            8) office ; break;;
            # Games
            9) games ; break;;
            # Develop
            10) develop ; break;;
            # Security software
            11) security ; break;;
            # Localized software
            12) bash ./bin/i18n.sh ; break;;
            # Return back
            $((${#items[@]}+1))) clear ; break 2;;
            *) clear ; break;;
        esac
    done
done
clear
