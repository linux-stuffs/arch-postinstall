#!/bin/bash
#
# Name: wizard-cutefish.sh
# Version: 0.1.2
# Provides: wizard-cutefish.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
# 	    cutefish.list sddm.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "#####################################"
    echo "#   WIZARD MODE - CUTEFISH DESKTOP  #"
    echo "#####################################"
    echo 
    echo "Wizard will automatically install software and CuteFish desktop"
    echo "with less users interactions."
    echo 
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/cutefish.list
		paclistins lists/sddm.list
		systemctl enable sddm.service
		installdone
		bash ./bin/i18n.sh; 
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear
