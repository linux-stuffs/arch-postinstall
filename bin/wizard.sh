#!/bin/bash
#
# Name: wizard.sh
# Version: 0.1.3
# Provides: wizard.sh wizard-basics.sh wizard-budgie.sh wizard-cinnamon.sh wizard-xfce4.sh 
#
# Requires: bash
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

clear
PS3=$'\nSelect item please: '
items=("Basics (no Desktop)" "Budgie Desktop" "Cinnamon Desktop" "CuteFish Desktop" "Deepin Desktop" "Enlightenment" "GNOME Desktop" "i3 Desktop" "KDE (Plasma) Desktop" "LXDE Desktop" "LXQt Desktop" "Mate Desktop" "Openbox Desktop" "UKUI Desktop" "Xfce4 Desktop" "Additional software (AUR)" "Localized software" "VirtualBox")

while true; do
    echo "#####################"
    echo "#   WIZARDs MODEs   #"
    echo "#####################"
    echo 
    echo "Wizard will automatically install software"
    echo "with less users interactions."
    echo
    select item in "${items[@]}" "Return back..."
    do
        case $REPLY in
            # Basics (no Desktop)
            1) bash bin/wizard-basics.sh; clear ; break;;
	    # Budgie
	    2) bash bin/wizard-budgie.sh; clear ; break;;
	    # Cinnamon 
	    3) bash bin/wizard-cinnamon.sh; clear ; break;;
	    # CuteFish
            4) bash bin/wizard-cutefish.sh; clear ; break;;
	    # Deepin
	    5) bash bin/wizard-deepin.sh; clear ; break;;
	    # Enlightenment
	    6) bash bin/wizard-enlightenment.sh; clear ; break;;
	    # GNOME
	    7) bash bin/wizard-gnome.sh;  clear ; break;;
	    # i3
	    8) bash bin/wizard-i3.sh; clear ; break;;
	    # KDE (Plasma)
	    9) bash bin/wizard-kde.sh; clear ; break;;
	    # LXDE
	    10) bash bin/wizard-lxde.sh; clear ; break;;
	    # LXQt
	    11) bash bin/wizard-lxqt.sh; clear ; break;;
	    # Mate
	    12) bash bin/wizard-mate.sh; clear ; break;;
	    # Openbox
	    13) bash bin/wizard-openbox.sh; clear ;break;;
	    # UKUI
	    14) bash bin/wizard-ukui.sh; clear ;break;;
	    # Xfce4
            15) bash bin/wizard-xfce4.sh; clear ; break;;
	    # Additional software (AUR)
	    16) bash bin/wizard-additional.sh; clear ; break;;
	    # Localized software
            17) bash ./bin/i18n.sh ; clear ; break;;
       	    # VirtualBox
	    18) bash bin/virtualbox.sh ; clear ; break;;
            # Quit
            $((${#items[@]}+1))) clear ; break 2;;
            *) clear ; break;
        esac
    done
done
clear
