#!/bin/bash
#
# Name: aur.sh
# Version: 0.1.5
# Provides: aur.sh, yay.sh, aur-wizard.list, aur-xfce4.list
#
# Requires: bash, grub-mkconfig, makepkg, sed, sudo
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh

aurwizard()
{
    clear
    echo "#######################"
    echo "#   AUR WIZARD MODE   #"
    echo "#######################"
    echo -e "\nWizard will automatically install AUR software with less"
    echo -e "users interactions.\n"
    echo -e "Will be installed these packages:\n"
    echo "yay, ttf-ms-fonts, sardi-icons, pamac-aur, "
    echo -e "archlinux-appstream-data-pamac, grub2-theme-arch-suse\n"
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	[yY] )
	    yaytest
	    yaylistins lists/aur-wizard.list
	    sed -i 's/#GRUB_THEME="\/path\/to\/gfxtheme"/GRUB_THEME="\/boot\/grub\/themes\/arch-suse\/theme\/theme.txt"/' /etc/default/grub
	    sed -i 's/^GRUB_GFXMODE=auto/#GRUB_GFXMODE=auto\nGRUB_GFXMODE=1024x768x24/' /etc/default/grub
	    grub-mkconfig -o /boot/grub/grub.cfg
	    installdone
	    clear
	    break;;
	[nN] ) clear; break;;
	* ) echo invalid response;;
    esac
}

clear
PS3=$'\nSelect item please: '
items=("Change username" "AUR Wizard" "Yay" "TTF MS Fonts" "Sardi icons" "Pamac (GUI)" "7-zip GUI" "OnlyOffice" "HardInfo" "AnyDesk" "TeamViewer" "Tux Racer" "Skype" "Graphical Grub" "Pace (pacman.conf editor)" "Stacer (System Optimizer)" "Timeshift (Restore utility)" "AUR Software for Xfce4")


while true; do
    echo "####################"
    echo "#   AUR programs   #"
    echo "####################"
    echo -e "\nAUR programs can not be installed under root user."
    
    if [ -z $AURUSER ]; then
	echo -e "You will must to select someone from these usernames:\n"
	grep '^wheel:.*$' /etc/group | cut -d: -f4
	echo ; read -p 'Type username and press Enter: ' AURUSER
	clear ; continue;
    else
	echo -e "\nSelected user: "$AURUSER"\n"
    fi

    select item in "${items[@]}" "Return back..."
    do
        case $REPLY in
        # Change username
	    1) unset AURUSER; clear; break;;
	    # AUR Wizard
	    2) aurwizard; break;;
	    # Yay
        3) yaytest ; break;;
        # TTF MS Fonts
        4) yayins ttf-ms-fonts ; break;;
	    # Sardi icons
        5) yayins sardi-icons ; break;;
	    # Pamac (GUI)
        6) yayins archlinux-appstream-data ; yayins pamac-aur ; break;;
	    # 7-zip GUI
        7) yayins p7zip-gui ; break;;
	    # OnlyOffice
        8) yayins onlyoffice-bin ; break;;
	    # HardInfo
	    9) yayins hardinfo-gtk3 ; break;;
	    # AnyDesk
	    10) yayins anydesk-bin ; break;;
	    # TeamViewer
	    11) yayins teamviewer ; break;;
	    # Extreme Tux Racer
	    12) yayins extremetuxracer ; break;;
	    # Skype
	    13) yayins skypeforlinux-stable-bin ; break;;
	    # Graphical Grub
	    14) 
		yayins grub2-theme-arch-suse
		sed -i 's/#GRUB_THEME="\/path\/to\/gfxtheme"/GRUB_THEME="\/boot\/grub\/themes\/arch-suse\/theme\/theme.txt"/' /etc/default/grub
		sed -i 's/^GRUB_GFXMODE=auto/#GRUB_GFXMODE=auto\nGRUB_GFXMODE=1024x768x24/' /etc/default/grub
		grub-mkconfig -o /boot/grub/grub.cfg
		clear
		break;;
	    # Pace (pacman.conf editor)
	    15) yayins pace ; break;;
	    # Stacer (System Optimizer)
	    16) yayins stacer ; break;;
	    # Timeshift (Restore utility)
	    17) yayins timeshift ; break;;
	    # Software for Xfce4
	    18)
		sudo -u $AURUSER -- bash -c 'cat lists/aur-xfce4.list | xargs yay -S --noconfirm;'
		sudo -u $AURUSER yay -Scc --aur --noconfirm
		clear
		break;;
        # Return back
        $((${#items[@]}+1))) echo "We're done!"; break 2;;
        *) echo "Ooops - unknown choice $REPLY"; break;
	esac
    done
done
clear
