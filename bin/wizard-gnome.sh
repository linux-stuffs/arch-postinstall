#!/bin/bash
#
# Name: wizard-gnome.sh
# Version: 0.1.2
# Provides: wizard-gnome.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
#	    gnome.list gdm.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "###################################"
    echo "#   WIZARD MODE - GNOME DESKTOP   #"
    echo "###################################"
    echo -e "\nWizard will automatically install software and GNOME desktop"
    echo -e "with less users interactions.\n"
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/gnome.list
		paclistins lists/gdm.list
		systemctl enable gdm.service
		installdone
		bash ./bin/i18n.sh; 
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear
