#!/bin/bash
#
# Name: yay.sh
# Version: 0.1.1
# Provides: yay.sh
#
# Requires: bash, git, makepkg
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

mkdir /tmp/myaur
cd /tmp/myaur
git clone https://aur.archlinux.org/yay.git
cd yay/
makepkg -sci --noconfirm
