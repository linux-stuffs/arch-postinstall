#!/bin/bash
#
# Name: latex.sh
# Version: 0.1.3
# Provides: latex.sh
#
# Requires: helper-installfunct.sh
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh

clear
while true; do
    echo "###################"
    echo "#   TeX & LaTeX   #"
    echo "###################"
    echo
    read -p "Do you want instal TeX & LaTex  (y/n) " yn
    case $yn in 
	[yY] ) 
	    paclistins lists/latex.list
	    break ;;
	[nN] ) break ;;
	* ) clear ;;
        esac
done
