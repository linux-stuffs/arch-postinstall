#!/bin/bash
#
# Name: wizard-cinnamon.sh
# Version: 0.1.2
# Provides: wizard-cinnamon.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
#	    cinnamon.list gdm.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "#####################################"
    echo "#   WIZARD MODE - CINNAMON DESKTOP  #"
    echo "#####################################"
    echo 
    echo "Wizard will automatically install software and Cinnamon desktop"
    echo "with less users interactions."
    echo 
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/cinnamon.list
		paclistins lists/gdm.list
		systemctl enable gdm.service
		installdone
		bash ./bin/i18n.sh; 
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear



