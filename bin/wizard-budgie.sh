#!/bin/bash
#
# Name: wizard-budgie.sh
# Version: 0.1.2
# Provides: wizard-budgie.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
#	    budgie.list lxdm.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "###################################"
    echo "#   WIZARD MODE - BUDGIE DESKTOP  #"
    echo "###################################"
    echo 
    echo "Wizard will automatically install software and Budgie desktop"
    echo "with less users interactions."
    echo 
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/budgie.list
		paclistins lists/lxdm.list
		systemctl enable lxdm.service
		installdone
		bash ./bin/i18n.sh; 
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear
