#!/bin/bash
#
# Name: desktop.sh
# Version: 0.1.2
# Provides: desktop.sh, budgie.list, cinnamon.list, cutefish.list, 
#           deepin.list, enlightenment.list, gdm.list, gnome.list, 
#           i3.list, kde.list, lightdm.list, lxde.list, lxdm.list, 
#           lxqt.list, mate.list, openbox.list, sddm.list, ukui.list, 
#           xfce4.list
#
# Requires: bash, cat, pacman, systemctl, xargs
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh

########################
#   DISPLAY MANAGERs   #
########################

displaymanagers()
{
    clear
    PS3=$'\nSelect item please: '
    items=("LightDM" "LightDM for Deepin" "LXDM" "SDDM" "GDM")
    while true; do
        echo "########################"
        echo "#   Display Managers   #"
        echo "########################"
        echo
        select item in "${items[@]}" "Return back..."
        do
            case $REPLY in
                # LightDM
                1) lightdm; break;;
                # LightDM for Deepin
                2) lightdm_deepin; break;;
                # LXDM
                3) lxdm; break;;
                # SDDM
                4) sddm; break;;
                # GDM
                5) gdm; break;;
                # Return back
                $((${#items[@]}+1))) clear ; exit;;
                * ) clear ; break;
            esac
        done
    done
    clear
}

gdm()
{
    clear
    while true; do
        echo "#################################################"
        echo "# Do you want to install GDM (Display Manager)? #"
        echo "#################################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                dmdisable
                paclistins lists/gdm.list
                systemctl enable gdm.service
                installdone ; clear
                exit;;
            [nN] ) clear; exit;;
            * ) clear ; break;;
        esac
    done
    clear
}

lightdm()
{
    clear
    while true; do
        echo "#####################################################"
        echo "# Do you want to install LightDM (Display Manager)? #"
        echo "#####################################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                dmdisable
                if $(pacman -Qi lightdm &>/dev/null); then
                    systemctl enable lightdm.service
                else
                    paclistins lists/lightdm.list
                    systemctl enable lightdm.service
                    sed -i 's/^\[Seat:\*\]/\[Seat:\*\]\ngreeter-setup-script=\/usr\/bin\/numlockx on/' /etc/lightdm/lightdm.conf
                fi
                installdone ; clear
                exit;;
            [nN] ) clear; exit;;
            *) clear ; break;;
        esac
    done
    clear
}

lightdm_deepin()
{   
    clear
    while true; do
        echo "##############################################"
        echo "# Do you want to install LightDM for Deepin? #"
        echo "##############################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                dmdisable
                if $(pacman -Qi lightdm &>/dev/null); then
                    systemctl enable lightdm.service
                else
                    paclistins lists/lightdm.list
                    systemctl enable lightdm.service
                    sed -i 's/^\[Seat:\*\]/\[Seat:\*\]\ngreeter-setup-script=\/usr\/bin\/numlockx on/' /etc/lightdm/lightdm.conf
                    sed -i 's/^\#greeter-session=example-gtk-gnome/\#greeter-session=example-gtk-gnome\ngreeter-session=lightdm-deepin-greeter/' /etc/lightdm/lightdm.conf
                fi
                installdone ; clear
                exit;;
            [nN] ) clear; exit;;
            * ) clear ; break;
        esac
    done
    clear
}

lxdm()
{
    clear
    while true; do
        echo "#####################################################"
        echo "# Do you want to install LXDM (Display Manager)? #"
        echo "#####################################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                dmdisable
                paclistins lists/lxdm.list
                systemctl enable lxdm.service
                installdone ; clear
                exit;;
            [nN] ) clear; exit;;
            * ) clear ; break;
        esac
    done
    clear
}

sddm()
{
    clear
    while true; do
        echo "#####################################################"
        echo "# Do you want to install SDDM (Display Manager)? #"
        echo "#####################################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                dmdisable
                paclistins lists/sddm.list
                systemctl enable sddm.service
                installdone ; clear
                exit;;
            [nN] ) clear; exit;;
            * ) clear ; break;
        esac
    done
    clear
}

################
#   DESKTOPs   #
################

budgie()
{
    clear
    while true; do
        echo "##################################"
        echo "# Do you want to install Budgie? #"
        echo "##################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/budgie.list
                installdone
                break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

cinnamon()
{
    clear
    while true; do
        echo "####################################"
        echo "# Do you want to install Cinnamon? #"
        echo "####################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/cinnamon.list
                installdone
                break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

cutefish()
{
    clear
    while true; do
        echo "####################################"
        echo "# Do you want to install CuteFish? #"
        echo "####################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/cutefish.list
                installdone
                break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

deepin()
{
    clear
    while true; do
        echo "##################################"
        echo "# Do you want to install Deepin? #"
        echo "##################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/deepin.list
                installdone
                break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

enlightenment()
{
    clear
    while true; do
        echo "#########################################"
        echo "# Do you want to install Enlightenment? #"
        echo "#########################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/enlightenment.list
                installdone
            break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

gnome()
{
    clear
    while true; do
        echo "##################################"
        echo "# Do you want to install GNOME? #"
        echo "##################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/gnome.list
                installdone
                break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

i3()
{
    clear
    while true; do
        echo "##############################"
        echo "# Do you want to install i3? #"
        echo "##############################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/i3.list
                installdone
                break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

kde()
{
    clear
    while true; do
        echo "########################################"
        echo "# Do you want to install KDE (Plasma)? #"
        echo "########################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/kde.list
                installdone
                break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

lxde()
{
    clear
    while true; do
        echo "#################################"
        echo "# Do you want to install LXDE? #"
        echo "#################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/lxde.list
                installdone
                break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

lxqt()
{
    clear
    while true; do
        echo "#################################"
        echo "# Do you want to install LXQt? #"
        echo "#################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/lxqt.list
                installdone
            break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

mate()
{
    clear
    while true; do
        echo "################################"
        echo "# Do you want to install Mate? #"
        echo "################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/mate.list
                installdone
            break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

openbox()
{
    clear
    while true; do
        echo "##################################"
        echo "# Do you want to install Openbox? #"
        echo "##################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/openbox.list
                installdone
                break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

ukui()
{
    clear
    while true; do
        echo "################################"
        echo "# Do you want to install UKUI? #"
        echo "################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/ukui.list
                installdone
                break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

xfce4()
{
    clear
    while true; do
        echo "#################################"
        echo "# Do you want to install XFCE4? #"
        echo "#################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] )
                paclistins lists/xfce4.list
                installdone
                break;;
            [nN] ) clear; break;;
            * ) clear ;;
        esac
    done
    clear
}

clear
PS3=$'\nSelect item please: '
items=("Budgie" "Cinnamon" "CuteFish" "Deepin" "Enlightenment" "GNOME" "i3" "KDE (Plasma)" "LXDE" "LXQt" "Mate" "Openbox" "UKUI" "Xfce4" "Display Managers...")

while true; do
    echo "####################################"
    echo "#   Archlinux Desktops Installer   #"
    echo "####################################"
    echo
    select item in "${items[@]}" "Return back..."
    do
        case $REPLY in
            # Budgie
            1) budgie; break;;
            # Cinnamon
            2) cinnamon; break;;
            # CuteFish
            3) cutefish; break;;
            # Deepin
            4) deepin; break;;
            # Enlightenment
            5) enlightenment; break;;
            # GNOME
            6) gnome; break;;
            # i3
            7) i3; break;;
            # KDE (Plasma)
            8) kde; break;;
            # LXDE
            9) lxde; break;;
            # LXQt
            10) lxqt; break;;
            # Mate
            11) mate; break;;
            # Openbox
            12) openbox; break;;
            # UKUI
            13) ukui; break;;
            # Xfce4
            14) xfce4; break;;
            # Display Managers
            15) displaymanagers; break;;
            # Return back
            $((${#items[@]}+1))) clear ; break 2;;
            * ) clear ;;
        esac
    done
done
clear
