#!/bin/bash
#
# Name: virtualbox.sh
# Version: 0.1.4
# Provides: virtualbox.sh
#
# Requires: helper-installfunct.sh
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh

clear
while true; do
    echo "###########################"
    echo "#   VirtualBox Settings   #"
    echo "###########################"
    echo
    read -p "Do you using VirtualBox? (y/n) " yn
    case $yn in
	[yY] )
	    anotheruser
	    pacins virtualbox-guest-utils
	    systemctl enable vboxservice.service
	    usermod -G vboxsf -a $AURUSER
	    break ;;
	[nN] ) break ;;
	* ) clear ;;
        esac
done
clear
