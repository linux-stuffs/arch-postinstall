#!/bin/bash
#
# Name: wizard-additional.sh
# Version: 0.1.5
# Provides: wizard-additional.sh
#
# Requires: bash grub-mkconfig makepkg sed sudo helper-installfunct.sh
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
PS3=$'\nSelect item please: '
items=("Change username" "ArchLinux Tweak Tool" "Pace (pacman.conf editor)" "Stacer (System Optimizer)" "Timeshift (Restore utility)" "HardInfo" "AnyDesk" "TeamViewer" "Skype" "7-zip GUI" "OnlyOffice" "Extreme Tux Racer")

while true; do
    echo "#################################"
    echo "#   Additional software (AUR)   #"
    echo "#################################"
    echo -e "\nAUR programs can not be installed under root user."
    
    if [ -z $AURUSER ]; then
	anotheruser
	clear
	continue;
    else
	echo -e "\nSelected user: "$AURUSER"\n"
    fi
    
    select item in "${items[@]}" "Return back..."
    do
        case $REPLY in
        # Change username
	    1) unset AURUSER; clear; break;;
	    
	    # ArchLinux Tweak Tool
        2) yayins archlinux-tweak-tool-git ; break;;
	    
	    # Pace (pacman.conf editor)
	    3) yayins pace ; break;;
	    
	    # Stacer (System Optimizer)
	    4) yayins stacer ; break;;
	    
	    # Timeshift (Restore utility)
	    5) yayins timeshift ; break;;
	    
	    # HardInfo
	    6) yayins hardinfo-gtk3 ; break;;

	    # AnyDesk
	    7) yayins anydesk-bin ; break;;

	    # TeamViewer
	    8) yayins teamviewer ; break;;
	    
	    # Skype
	    9) yayins skypeforlinux-stable-bin ; break;;
	    
	    # 7-zip GUI
        10) yayins p7zip-gui ; break;;
	    
	    # OnlyOffice
        11) yayins onlyoffice-bin ; break;;
	    
	    # Extreme Tux Racer
	    12) yayins extremetuxracer ; break;;
	    
	    # Return back
        $((${#items[@]}+1))) echo "We're done!"; break 2;;
        
        *) clear ; break;
        esac
    done
done
clear
