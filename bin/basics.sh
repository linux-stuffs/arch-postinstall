#!/bin/bash
#
# Name: basics.sh
# Version: 0.1.3
# Provides: basics.sh, basics.list
#
# Requires: bash, cat, xargs, pacman, makepkg, settings.sh
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh

basicprograms()
{
    clear
    while true; do
        echo "###################################################"
        echo "# Do you want to install basics console software? #"
        echo "###################################################"
        echo 
        read -p "Do you want to proceed? (y/n) " yn
        case $yn in 
            [yY] ) 
	    paclistins lists/basics.list
	    installdone ; clear
            break;;
            [nN] ) clear ; break;;
            * ) clear;;
        esac
    done
}

PS3=$'\nSelect item please: '
items=("Settings" "Install basic programs" "VirtualBox")
clear

while true; do
    echo "#####################"
    echo "#   Basics stuffs   #"
    echo "#####################"
    echo
    select item in "${items[@]}" "Return back..."
    do
        case $REPLY in
	    # Basics settings
	    1) bash bin/settings.sh; break;;
	    # Install Basics programs
	    2) basicprograms; break;;
    	    # VirtualBox
	    3) bash bin/virtualbox.sh; break;;
            # Return back
            $((${#items[@]}+1))) break 2;;
            *) clear ; break;
        esac
    done
done
clear

