#!/bin/bash
#
# Name: wizard-kde.sh
# Version: 0.1.3
# Provides: wizard-kde.sh
#
# Requires: helper-installfunct.sh helper-wizards.sh 
#	    kde.list sddm.list
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh
source ./incl/helper-wizards.sh

clear
while true; do
    echo "##########################################"
    echo "#   WIZARD MODE - KDE (Plasma) DESKTOP   #"
    echo "##########################################"
    echo -e "\nWizard will automatically install software and KDE desktop"
    echo -e "with less users interactions.\n"
    read -p "Do you want to proceed? (y/n) " yn
    case $yn in 
	    [yY] )
		anotheruser
		wizbasics
		paclistins lists/kde.list
		paclistins lists/sddm.list
		systemctl enable sddm.service
		clear
		wizkdeadd
		installdone
		bash ./bin/i18n.sh; 
		break;;
	    [nN] ) clear; exit;;
	    * ) echo invalid response;;
    esac
done
clear
