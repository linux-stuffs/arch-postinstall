#!/bin/bash
#
# Name: i18n.sh
# Version: 0.1.3
# Provides: i18n.sh, i18n-cs.list, i18n-de.list, i18n-fr.list, 
#	    i18n-sk.list
#
# Requires: bash, cat, xargs, pacman
# Devs: Pavel Sibal <entexsoft@gmail.com>

source ./incl/helper-installfunct.sh

localize()
{
    soft=("man-pages" "hunspell" "firefox" "thunderbird" "libreoffice-still" "libreoffice-fresh" "tesseract")
    for i in ${!soft[@]}; do
	if [ -z "${i18n[$i]}" ]; then
	    continue
	fi
	if $(pacman -Qi ${soft[$i]} &>/dev/null); then
	    clear
	else
	    continue
	fi
	if $(pacman -Qi ${i18n[$i]} &>/dev/null); then
	    continue
	else
	    pacman -S "${i18n[$i]}" --noconfirm;
	fi
    done
    unset i ; unset soft ; unset i18n ; clear ;
    installdone
    clear
    exit
}

VCONSOLE="/etc/vconsole.conf"
LOCALEGEN="/etc/locale.gen"
LOCALECFG="/etc/locale.conf"

touch $VCONSOLE
touch $LOCALECFG
if [[ $(grep -L "^en_*" $LOCALEGEN) ]]; then
    echo -e "en_US.UTF-8 UTF-8" >> $LOCALEGEN
fi
PS3=$'\nSelect item please: '
items=("Czech" "Slovak" "German" "French")
clear

while true; do
    echo "##################################"
    echo "#   Translation & Localization   #"
    echo -e "##################################\n"

    select item in "${items[@]}" "Return back..."
    do
        case $REPLY in
	    # Czech
            1) i18n=("man-pages-cs" "hunspell-cs" "firefox-i18n-cs" "thunderbird-i18n-cs" "libreoffice-still-cs" "libreoffice-fresh-cs" "tesseract-data-ces")
	       anotheruser
	       yayins hunspell-cs
	       localectl --no-convert set-x11-keymap cz,us "" "" grp:win_space_toggle
	       if [[ $(grep -L "^KEYMAP=cz*" $VCONSOLE) ]]; then
		    echo -e "KEYMAP=cz-qwertz" > $VCONSOLE
	       fi
	       if [[ $(grep -L "^FONT=Lat2-Terminus16" $VCONSOLE) ]]; then
		    echo "FONT=Lat2-Terminus16" >> $VCONSOLE
	       fi
	       if [[ $(grep -L "^cs_CZ*" $LOCALEGEN) ]]; then
		    echo -e "cs_CZ.UTF-8 UTF-8" >> $LOCALEGEN
	       fi
	       if [[ $(grep -L "^LANG=cs_CZ*" $LOCALECFG) ]]; then
		    echo -e "LANG=cs_CZ.UTF-8" > $LOCALECFG
	       fi
	       locale-gen
	       localize ;;
	    # Slovak
	    2) i18n=("man-pages-cs" "" "firefox-i18n-sk" "thunderbird-i18n-sk" "libreoffice-still-sk" "libreoffice-fresh-sk" "")
	       anotheruser
	       yayins hunspell-sk
	       localectl --no-convert set-x11-keymap sk,us "" "" grp:win_space_toggle
	       if [[ $(grep -L "^KEYMAP=sk*" $VCONSOLE) ]]; then
		    echo -e "KEYMAP=sk-qwertz" > $VCONSOLE
	       fi
	       if [[ $(grep -L "^FONT=Lat2-Terminus16" $VCONSOLE) ]]; then
		    echo "FONT=Lat2-Terminus16" >> $VCONSOLE
	       fi
	       if [[ $(grep -L "^sk_SK*" $LOCALEGEN) ]]; then
		    echo -e "sk_SK.UTF-8 UTF-8" >> $LOCALEGEN
	       fi
	       if [[ $(grep -L "^LANG=sk_SK*" $LOCALECFG) ]]; then
		    echo -e "LANG=sk_SK.UTF-8" > $LOCALECFG
	       fi
	       locale-gen
	       localize;;
	    # German
	    3) i18n=("man-pages-de" "hunspell-de" "firefox-i18n-de" "thunderbird-i18n-de" "libreoffice-still-de" "libreoffice-fresh-de" "tesseract-data-deu")
	       localectl --no-convert set-x11-keymap de,us "" "" grp:win_space_toggle
	       if [[ $(grep -L "^KEYMAP=de*" $VCONSOLE) ]]; then
		    echo -e "KEYMAP=de-latin1" > $VCONSOLE
	       fi
	       if [[ $(grep -L "^FONT=Lat2-Terminus16" $VCONSOLE) ]]; then
		    echo "FONT=Lat2-Terminus16" >> $VCONSOLE
	       fi
	       if [[ $(grep -L "^de_DE*" $LOCALEGEN) ]]; then
		    echo -e "de_DE.UTF-8 UTF-8" >> $LOCALEGEN
	       fi
	       if [[ $(grep -L "^LANG=de_DE*" $LOCALECFG) ]]; then
		    echo -e "LANG=de_DE.UTF-8" > $LOCALECFG
	       fi
	       locale-gen
	       localize;;
	    # French
	    4) i18n=("man-pages-fr" "hunspell-fr" "firefox-i18n-fr" "thunderbird-i18n-fr" "libreoffice-still-fr" "libreoffice-fresh-fr" "")
	       localectl --no-convert set-x11-keymap fr,us "" "" grp:win_space_toggle
	       if [[ $(grep -L "^KEYMAP=fr*" $VCONSOLE) ]]; then
		    echo -e "KEYMAP=fr" > $VCONSOLE
	       fi
	       if [[ $(grep -L "^FONT=gr737c" $VCONSOLE) ]]; then
		    echo "FONT=gr737c-8x14" >> $VCONSOLE
	       fi
	       if [[ $(grep -L "^de_DE*" $LOCALEGEN) ]]; then
		    echo -e "fr_FR.UTF-8 UTF-8" >> $LOCALEGEN
	       fi
	       if [[ $(grep -L "^LANG=fr_*" $LOCALECFG) ]]; then
		    echo -e "LANG=fr_FR.UTF-8" > $LOCALECFG
	       fi
       	       if [[ $(grep -L "^LC_ADDRESS=fr_*" $LOCALECFG) ]]; then
		    echo -e "LC_ADDRESS=fr_FR.UTF-8" >> $LOCALECFG
	       fi
       	       if [[ $(grep -L "^LC_IDENTIFICATION=fr_*" $LOCALECFG) ]]; then
		    echo -e "LC_IDENTIFICATION=fr_FR.UTF-8" >> $LOCALECFG
	       fi
	       if [[ $(grep -L "^LC_MEASUREMENT=fr_*" $LOCALECFG) ]]; then
		    echo -e "LC_MEASUREMENT=fr_FR.UTF-8" >> $LOCALECFG
	       fi
	       if [[ $(grep -L "^LC_MONETARY=fr_*" $LOCALECFG) ]]; then
		    echo -e "LC_MONETARY=fr_FR.UTF-8" >> $LOCALECFG
	       fi
	       if [[ $(grep -L "^LC_NAME=fr_*" $LOCALECFG) ]]; then
		    echo -e "LC_NAME=fr_FR.UTF-8" >> $LOCALECFG
	       fi
	       if [[ $(grep -L "^LC_NUMERIC=fr_*" $LOCALECFG) ]]; then
		    echo -e "LC_NUMERIC=fr_FR.UTF-8" >> $LOCALECFG
	       fi
	       if [[ $(grep -L "^LC_PAPER=fr_*" $LOCALECFG) ]]; then
		    echo -e "LC_PAPER=fr_FR.UTF-8" >> $LOCALECFG
	       fi
	       if [[ $(grep -L "^LC_TELEPHONE=fr_*" $LOCALECFG) ]]; then
		    echo -e "LC_TELEPHONE=fr_FR.UTF-8" >> $LOCALECFG
	       fi
	       if [[ $(grep -L "^LC_TIME=fr_*" $LOCALECFG) ]]; then
		    echo -e "LC_TIME=fr_FR.UTF-8" >> $LOCALECFG
	       fi
	       locale-gen
	       localize;;
           # Return back
           $((${#items[@]}+1))) unset VCONSOLE ; unset LOCALEGEN ; unset LOCALECFG ; clear ; break 2;;
           *) clear ; break;
        esac
    done
done


