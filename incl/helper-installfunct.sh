#!/bin/bash
#
# Name: helper-installfunct.sh
# Version: 0.1.3
#
# Requires: bash, makepkg, sed, sudo
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

paclistins()
{
    if $(cat $1 | xargs pacman -Qi &>/dev/null); then
        clear ; echo -e "\n!!! Packages from "$1" is installed !!!\n\n" ; sleep 1 
        clear
    else
        cat $1 | xargs pacman -S --noconfirm;
        clear
    fi
}

pacins()
{
    if $(pacman -Qi $1 &>/dev/null); then
	clear ; echo -e "\n!!! Package "$1" is installed !!!\n\n" ; sleep 1 
	clear ;
    else
	pacman -S $1 --noconfirm
	clear
    fi
}

yaytest()
{
    if $(pacman -Qi yay &>/dev/null); then
	clear ;
    else
	sudo -u $AURUSER bash ./bin/yay.sh
	rm -rf /tmp/myaur
	pacman -R go --noconfirm
	clear
    fi
}

yayins()
{
    yaytest
    if $(pacman -Qi $1 &>/dev/null); then
	clear ; echo -e "\n!!! Package "$1" is installed !!!\n\n" ; sleep 1 
	clear ;
    else
	sudo -u $AURUSER yay -S $1 --noconfirm
	sudo -u $AURUSER yay -Scc --aur --noconfirm
	clear
    fi
}

yaylistins()
{
    yaytest
    if $(cat $1 | xargs pacman -Qi &>/dev/null); then
	clear ; echo -e "\n!!! Packages from "$1" is installed !!!\n\n" ; sleep 1
        clear ; 
    else
	sudo -u $AURUSER bash ./bin/yaylistins.sh $1
        clear
    fi
}

anotheruser()
{
    if [[ -z "${AURUSER}" ]]; then
	echo -e "\nSome programs can not be installed under root user."
	echo -e "You will must to select someone from these usernames:\n"
	grep '^wheel:.*$' /etc/group | cut -d: -f4
	echo 
	read -p 'Type valid Username and press Enter: ' AURUSER
	echo
	export AURUSER
    fi
}

installdone()
{
    echo -e "\nInstallation is done...\n\n"
    read -p 'Press Enter for continue...'
}

dmcheck()
{
    if $(pacman -Qi gdm &>/dev/null); then
	return 1
    elif $(pacman -Qi lightdm &>/dev/null); then
	return 1
    elif $(pacman -Qi lxdm &>/dev/null); then
	return 1
    elif $(pacman -Qi sddm &>/dev/null); then
	return 1
    else
	return 0
    fi
}

dmdisable()
{
    if $(pacman -Qi gdm &>/dev/null); then
	systemctl disable gdm.service
    fi
    if $(pacman -Qi lightdm &>/dev/null); then
	systemctl enable lightdm.service
    fi
    if $(pacman -Qi lxdm &>/dev/null); then
	systemctl enable lxdm.service
    fi
    if $(pacman -Qi sddm &>/dev/null); then
	systemctl enable sddm.service
    fi
}
