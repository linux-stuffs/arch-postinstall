#!/bin/bash
#
# Name: helper-installfunct.sh
# Version: 0.1.5
#
# Requires: bash, makepkg, sed, sudo
#
# Devs: Pavel Sibal <entexsoft@gmail.com>

libreofficechk()
{
    while true; do
        echo -e "\nWhich version of LibreOffice Suite?"
        echo -e "\n1) Stable\n2) Fresh\n3) None\n"
        read -p "Which version of LibreOffice Suite? " yn
        case $yn in 
            1) pacins libreoffice-still ; break ;;
            2) pacins libreoffice-fresh ; break ;;
            3) break ;;
            * ) clear ;;
        esac
    done
}

wizbasics()
{
    pacman -Syyu --noconfirm
    paclistins lists/basics.list
    paclistins lists/xorg-base.list
    paclistins lists/xorg-programs.list
    paclistins lists/fonts.list
    paclistins lists/deskprograms.list
    
    while true; do
        echo "-- Basics Network software --"
        echo "discord, filezilla, firefox, qbittorrent, telegram, thunderbird"
        echo ; read -p "Do you want to install Network software (y/n) " yn
        case $yn in 
            [yY] ) paclistins lists/networksoft.list ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    
    while true; do
        echo "-- Basics Multimedia Stuffs --"
        echo "(codecs and multimedia software like audacious, audacity"
        echo "gstreamer, handbrake, smplayer, mencoder, vlc)"
        echo ; read -p "Do you want to install Basic Multimedia stuffs? (y/n) " yn
        case $yn in 
            [yY] ) paclistins lists/multimedia-basics.list ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    
    while true; do
        echo "-- Audio station --"
        echo "ardour, cadence, catia, guitarix, mixxx, muse, musescore, qjackctl"
        echo ; read -p "Do you want to install Audio station? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/multimedia-audiostation.list
                yayins rakarrack-plus
                groupadd realtime
                echo -e "\nUser, which will use the Audio Station software"
                echo -e "must to be into 'realtime' group."
                echo ; read -p "Please type the username " rtuser
                usermod -G realtime -a $rtuser
                break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    
    while true; do
        echo "-- Basic graphics software --"
        echo "gimp, gimp-plugin-gmic, inkscape"
        echo ; read -p "Do you want to install Graphics software? (y/n) " yn
        case $yn in 
            [yY] ) paclistins lists/graphicssoft.list ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    
    while true; do
        echo ; read -p "Do you want to install Office & Productivity? (y/n) " yn
        case $yn in 
            [yY] ) 
            libreofficechk ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    
    while true; do
        echo "-- Games --"
        echo "aisleriot, armagetronad, astromenace, chromium-bsu, granatier, mahjongg, supertuxkart"
        echo ; read -p "Do you want to install some Games? (y/n) " yn
        case $yn in 
            [yY] ) paclistins lists/games.list ; break;;
            [nN] ) break;;
            * ) clear ;;
        esac
    done

    while true; do
        echo "-- Basic Develop utils --"
        echo "code, geany, meld, sqlitebrowser"
        echo ; read -p "Do you want to install some Develop utils? (y/n) " yn
        case $yn in 
            [yY] ) paclistins lists/develop.list ; break;;
            [nN] ) break;;
            * ) clear ;;
        esac
    done

    while true; do
        echo "-- Security software --"
        echo "encryptpad, gufw, keepass, kleopatra, openstego, onionshare, veracrypt"
        echo ; read -p "Do you want to install some Security software? (y/n) " yn
        case $yn in 
            [yY] ) 
                paclistins lists/security.list
                yayins encryptpad
                yayins openstego
                break;;
            [nN] ) break;;
            * ) clear ;;
        esac
    done

    clear
    echo "#################################################"
    echo "#   Installing yay and basics stuffs from AUR   #"
    echo -e "#################################################\n"
    yaylistins lists/aur-wizard.list
    sed -i 's/#GRUB_THEME="\/path\/to\/gfxtheme"/GRUB_THEME="\/boot\/grub\/themes\/arch-suse\/theme\/theme.txt"/' /etc/default/grub
    sed -i 's/^GRUB_GFXMODE=auto/#GRUB_GFXMODE=auto\nGRUB_GFXMODE=1024x768x24/' /etc/default/grub
    grub-mkconfig -o /boot/grub/grub.cfg
    sudo -u $AURUSER yay -Scc --noconfirm

    while true; do
        echo -e "\nPamac (Package Manager)?"
        echo -e "\n1) Pamac (AUR)\n2) Pamac (AUR, Flatpak, Snap)\n3) None\n"
        read -p "Which version of Pamac? " yn
        case $yn in 
            1) yayins pamac-aur ; break ;;
            2) yayins pamac-all ; break ;;
            3) break ;;
            * ) clear ;;
        esac
    done

}

wizkdeadd()
{
    while true; do
        read -p "Do you want to install KDE Accessibility (y/n) " yn
        case $yn in 
            [yY] ) pacins kde-accessibility-meta ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    while true; do
        read -p "Do you want to install KDE Education (y/n) " yn
        case $yn in 
            [yY] ) pacins kde-education-meta ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    while true; do
        read -p "Do you want to install KDE Games (y/n) " yn
        case $yn in 
            [yY] ) pacins kde-games-meta ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    while true; do
        read -p "Do you want to install KDE Graphics (y/n) " yn
        case $yn in 
            [yY] ) pacins kde-graphics-meta ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    while true; do
        read -p "Do you want to install KDE Multimedia (y/n) " yn
        case $yn in 
            [yY] ) pacins kde-multimedia-meta ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    while true; do
        read -p "Do you want to install KDE Network (y/n) " yn
        case $yn in 
            [yY] ) pacins kde-network-meta ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    while true; do
        read -p "Do you want to install KDE PIM (y/n) " yn
        case $yn in 
            [yY] ) pacins kde-pim-meta ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    while true; do
        read -p "Do you want to install KDE System Utilities (y/n) " yn
        case $yn in 
            [yY] ) pacins kde-system-meta ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    while true; do
        read -p "Do you want to install KDE Utilities (y/n) " yn
        case $yn in 
            [yY] ) pacins kde-utilities-meta ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    while true; do
        read -p "Do you want to install KDE SDK (y/n) " yn
        case $yn in 
            [yY] ) pacins kdesdk ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
    while true; do
        read -p "Do you want to install KDevelop (y/n) " yn
        case $yn in 
            [yY] ) pacins kdevelop-meta ; break ;;
            [nN] ) break ;;
            * ) clear ;;
        esac
    done
}
